$(document).ready(function(){
    var tumpukan = [] // Array Kosong
    $("#data-push").focus();

    $("#tombol-push").click(function(){
        if ($.trim($("#data-push").val()) == "") {
            alert("Data tidak boleh kosong");
            return;
        }

        // Simpan ke tumpukan
        tumpukan.push($.trim($("#data-push").val()));

        // Kosongkan Field teks
        $("#info-kosong").val("");
        $("#data-push").focus();

        // Visualisasikan
        $("#info-kosong").hide();
        $("#data-tumpukan").empty();

        $.each(tumpukan, function(indeks, nilai){
            $("#data-tumpukan").prepend("<div>" + nilai + "</div>")
        });
    });
    $("#tombol-pop").click(function(){
        var jumData = tumpukan.length;
        if (jumData == 0) {
            alert("Tumpukan sudah kosong");
            return;
        };

        //Hapus dari tumpukan
        var data = tumpukan.pop();

        // Visualisasikan
        if (tumpukan.length == 0) {
            $("#info-kosong").show();
            $("#data-tumpukan").empty();
        }
        else {
            $("#info-kosong").hide();
            $("#data-tumpukan").empty();

            $.each(tumpukan, function(indeks, nilai){
                $("#data-tumpukan").prepend("<div>" + nilai + "</div>")
            });
        };
    });
});